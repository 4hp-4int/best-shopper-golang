TODO: Eventually kick off NUM_INSTANCES based on config values.

## Environment Setup

1. Create a file named `config.env` based on the `config.env_example` file.
2. Fill in the values for the following:
    * WALMART_USERNAME
    * WALMART_PASSWORD (Base64Encoded)
    * PAYMENT_CCV (Base64Encoded)
3. Script relies on saved payment method/delivery address