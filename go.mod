module gitlab.com/shopper

go 1.15

require (
	github.com/chromedp/cdproto v0.0.0-20201204063249-be40c824ad18
	github.com/chromedp/chromedp v0.5.4
	github.com/spf13/viper v1.7.1
)
