package main

import (
	"context"
	"encoding/base64"
	"flag"
	"fmt"
	"log"
	"strings"

	"github.com/chromedp/chromedp"

	"gitlab.com/shopper/util"
)

type products []string

func (p *products) String() string {
	return fmt.Sprint(*p)
}

// Set required for parsing -u argument properly.
func (p *products) Set(value string) error {
	for _, url := range strings.Split(value, " ") {
		*p = append(*p, url)
	}

	return nil
}

var urlFlag products
var wsURL string

func init() {
	flag.Var(&urlFlag, "u", "Provide a product page url(s).")

}

func addToCart(c util.Config) chromedp.Tasks {
	var subTotal string
	return chromedp.Tasks{
		chromedp.Navigate("https://www.walmart.com/ip/Sony-PlayStation-5-Digital-Edition/493824815"),
		chromedp.WaitVisible(c.WalmartAddToCartSelector),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("Add to cart button visible.")
			return nil
		}),
		chromedp.Click(c.WalmartAddToCartSelector, chromedp.NodeVisible),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("Button clicked!")
			return nil
		}),
		chromedp.WaitVisible(`#cart-root-container-content-skip > div:nth-child(1) > div > div.Cart-PACModal.standard-pac.pac-added.pac-new-ny.no-price-fulfillment.pac-vanilla-hf > div > div > div > div > div.Cart-PACModal-Body-right-rail.Grid-col.u-size-1.u-size-1-2-m.u-size-1-2-l > div > div > div.Grid-col.u-size-1-2.cart-pos.cart-phase-2design.cart-pos-sticky.pos-new-ny-styling > div:nth-child(1) > div > div:nth-child(4) > div.order-summary-grand-total.order-summary-line`),
		chromedp.Text(`#cart-root-container-content-skip > div:nth-child(1) > div > div.Cart-PACModal.standard-pac.pac-added.pac-new-ny.no-price-fulfillment.pac-vanilla-hf > div > div > div > div > div.Cart-PACModal-Body-right-rail.Grid-col.u-size-1.u-size-1-2-m.u-size-1-2-l > div > div > div.Grid-col.u-size-1-2.cart-pos.cart-phase-2design.cart-pos-sticky.pos-new-ny-styling > div:nth-child(1) > div > div:nth-child(4) > div.order-summary-grand-total.order-summary-line`, &subTotal),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("%s", subTotal)
			return nil
		}),
		chromedp.WaitVisible(`#cart-root-container-content-skip > div:nth-child(1) > div > div.Cart-PACModal.standard-pac.pac-added.pac-new-ny.no-price-fulfillment.pac-vanilla-hf > div > div > div > div > div.Cart-PACModal-Body-right-rail.Grid-col.u-size-1.u-size-1-2-m.u-size-1-2-l > div > div > div.Grid-col.u-size-1-2.pos-actions-container > div.cart-pos-main-actions.s-margin-top > div.new-ny-styling.cart-pos-proceed-to-checkout > div > button.button.ios-primary-btn-touch-fix.hide-content-max-m.checkoutBtn.button--primary`),
		chromedp.Click(`#cart-root-container-content-skip > div:nth-child(1) > div > div.Cart-PACModal.standard-pac.pac-added.pac-new-ny.no-price-fulfillment.pac-vanilla-hf > div > div > div > div > div.Cart-PACModal-Body-right-rail.Grid-col.u-size-1.u-size-1-2-m.u-size-1-2-l > div > div > div.Grid-col.u-size-1-2.pos-actions-container > div.cart-pos-main-actions.s-margin-top > div.new-ny-styling.cart-pos-proceed-to-checkout > div > button.button.ios-primary-btn-touch-fix.hide-content-max-m.checkoutBtn.button--primary`, chromedp.NodeVisible),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("Item added to cart!")
			return nil
		}),
	}
}

func signIn(c util.Config) chromedp.Tasks {
	//Decode config.WalmartPassword value
	walmartPassword, e := base64.StdEncoding.DecodeString(c.WalmartPassword)
	if e != nil {
		log.Fatal(e)
	}
	return chromedp.Tasks{
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("Attempting Sign-in")
			return nil
		}),
		chromedp.SendKeys(`#sign-in-email`, c.WalmartUsername, chromedp.ByID),
		chromedp.SendKeys(`input[name="password"]`, string(walmartPassword)),
		chromedp.Click(`button[data-tl-id="signin-submit-btn"]`),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("Signed in!")
			return nil
		}),
	}
}

func checkOut(c util.Config) chromedp.Tasks {
	//Decode CCV value
	// ccv, e := base64.StdEncoding.DecodeString(c.PaymentCCV)
	// if e != nil {
	//    	log.Fatal(e)
	// }
	var thankYouMessage string
	return chromedp.Tasks{
		// chromedp.WaitVisible(c.WalmartCheckoutContinueSelector),
		// chromedp.Sleep(1),
		// chromedp.Click(c.WalmartCheckoutContinueSelector, chromedp.NodeVisible),
		// chromedp.ActionFunc(func(context.Context) error {
		// 	log.Printf("Reviewing Delivery....")
		// 	return nil
		// }),
		// chromedp.WaitVisible(c.WalmartCheckoutDeliverySelector),
		// chromedp.Sleep(2.0),
		// chromedp.Click(c.WalmartCheckoutDeliverySelector, chromedp.NodeVisible),
		// chromedp.ActionFunc(func(context.Context) error {
		// 	log.Printf("Entering Payment Information...")
		// 	return nil
		// }),
		// chromedp.WaitVisible(c.WalmartCCVInputSelector),
		// chromedp.Sleep(1.0),
		// chromedp.SendKeys(c.WalmartCCVInputSelector, string(ccv)),
		// chromedp.Click(c.WalmartReviewOrderSelector),
		// chromedp.ActionFunc(func(context.Context) error {
		// 	log.Printf("Reviewing Order...")
		// 	return nil
		// }),
		chromedp.WaitVisible(c.WalmartPlaceOrderSelector),
		chromedp.Click(c.WalmartPlaceOrderSelector, chromedp.NodeVisible),
		chromedp.Text(c.WalmartThankYouMessageSelector, &thankYouMessage),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("%s", thankYouMessage)
			return nil
		}),
	}
}

var flagDevToolWsURL = flag.String("ws", "", "DevTools WebSsocket URL")

func main() {
	flag.Parse()
	if *flagDevToolWsURL == "" {
		log.Fatal("must specify -ws")
	}

	config, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config:", err)
	}

	// create allocator context for use with creating a browser context later
	allocatorContext, cancel := chromedp.NewRemoteAllocator(context.Background(), *flagDevToolWsURL)
	defer cancel()

	// create context
	ctxt, cancel := chromedp.NewContext(allocatorContext)
	defer cancel()

	//Attempt a Checkout Flow
	if err := chromedp.Run(ctxt,
		addToCart(config),
		signIn(config),
	); err != nil {
		log.Fatal((err))
	}

}

//Process
// Reload page until Add to Cart button is Visible
//Once it is visible proceed addToCart/SignIn/Checkout
