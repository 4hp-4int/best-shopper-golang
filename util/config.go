package util

import "github.com/spf13/viper"

// Config Object
type Config struct {
	WalmartUsername	string `mapstructure:"WALMART_USERNAME"`
	WalmartPassword string `mapstructure:"WALMART_PASSWORD"`
	PaymentCCV string `mapstructure:"PAYMENT_CCV_VALUE"`
	WalmartAddToCartSelector string `mapstructure:"WALMART_ADD_TO_CART_SEL"`
	WalmartCheckoutSelector string `mapstructure:"WALMART_CHECKOUT_SEL"`
	WalmartCheckoutContinueSelector string `mapstructure:"WALMART_CHECKOUT_CONTINUE_SEL"`
	WalmartCheckoutDeliverySelector string `mapstructure:"WALMART_CHECKOUT_DELIVERY_SEL"`
	WalmartCCVInputSelector string `mapstructure:"WALMART_CCV_INPUT_SEL"`
	WalmartReviewOrderSelector string `mapstructure:"WALMART_REVIEW_ORDER_SEL"`
	WalmartPlaceOrderSelector string `mapstructure:"WALMART_PLACE_ORDER_SEL"`
	WalmartThankYouMessageSelector string `mapstructure:"WALMART_THANK_YOU_MESSAGE_SEL"`

}

// LoadConfig loads the env file
func LoadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("config")
	viper.SetConfigType("env")

    viper.AutomaticEnv()

    err = viper.ReadInConfig()
    if err != nil {
        return
    }

    err = viper.Unmarshal(&config)
    return
	
}